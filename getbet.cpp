/*  
Le but de cet exercice est de connaître la valeur d'un des bits qui compose un entier non signé.
Spécification:
    *Value est un entier non signé de 32 bits
    *pos est l'index du bit à récupérer dans value. La ôstion 0 correspond au premier bit

Implémentez la méthode getBit() afin qu'elle retourne la valeur du bit demander.

exemple:
si value = 5(soit 0101 en binaire), alors:

getbit(5,0) retourne 1;
getbit(5,1) retourne 0
getbit(5, 2) retourne 1

*/

/* ************************************************************* */


#include<iostream>
#include<vector>
#include<string>

using namespace std;


int getbit(int Value, int pos){
vector<int> bit;
auto r=0;
while(Value >= 2){
    r = Value%2;
    bit.push_back(r);
    Value = (Value - r)/2;
}

bit.push_back(Value);// On ajoute le dernier element qui est sortie du while

    /*for(auto i:bit){ 
        cout<<"digit:"<<i<<endl;}*/

    
    return bit[pos];
}



int main(){

    cout<<" 5 position 1:"<<getbit(5,0)<<endl;

    return 0;
}